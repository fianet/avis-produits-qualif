<?

	require_once ('include/tools.php');

	function getAuthorizedSites(&$sites, $needle = null) {
		$query = "
			SELECT DISTINCT S.SiteID
			FROM
				/* Sites Actifs */
				fianet2..Site S
					JOIN rating..RatingSiteAccess RSA
						ON S.SiteID = RSA.SiteID
					JOIN fianet2..SiteGarantie SG
						ON S.SiteID = SG.SiteID AND GarantieID = 7
					JOIN fianet2..SiteInfo SI
						ON S.SiteID = SI.SiteID AND InfoID = 32 AND ValueListe != 1
					JOIN rating..RatingSite RS
						ON S.SiteID = RS.SiteID
					/* Souscription a l'option avis produits */
					JOIN fianet2..vSiteExtranetDroitAll SED
						ON SED.SiteID = S.SiteID AND SED.OptionID = 15
			WHERE EXISTS (
									SELECT 1
									FROM rating..Produit P
											JOIN rating..ProduitStatSite PSS
													ON PSS.ProduitClientID = P.ProduitClientID AND PSS.SiteID = P.SiteID
									WHERE P.SiteID = S.SiteID
								)
		";
		if ($needle) {
			$query .= " AND S.SiteID = ${needle}";
		}
		$results = doRequest($query);

		if ($results) {
			$sites = array_column($results, 'SiteID');
		}
	}


	function getSites(&$input) {
		getAuthorizedSites($sites);
		// var_export($sites);
		$siteStr = " IN (". implode(',', $sites).")";

		$query = "
			SELECT S.SiteID id, S.SiteName text, SSL.UrlLogo img
			FROM fianet2..Site S
				LEFT JOIN fianet2..SACSiteLogo SSL
					ON SSL.SiteID = S.SiteID
			WHERE S.SiteID {$siteStr}
			ORDER BY S.SiteName ASC
		";

		return $query;
	}

	function getProducts(&$input) {
		$siteid = $input['SiteID'];
		$needleQry = '';

		if (!empty($input['needle'])) {
			$needle = strtolower($input['needle']);
			$needleQry = " LOWER(P.ProduitClientID) LIKE '{$needle}%' AND ";
		}

		$query = "
			SELECT DISTINCT P.ProduitClientID id, P.ProduitClientID text
			FROM rating..Produit P
				JOIN rating..ProduitStatSite PSS
					ON PSS.ProduitClientID = P.ProduitClientID AND PSS.SiteID = P.SiteID
			WHERE {$needleQry} P.SiteID = {$siteid}
			ORDER BY P.ProduitClientID ASC
		";
		return $query;
	}

	function getGenerateAvis(&$input) {
		$data = array(
			'sid' => $input['SiteID'],
			'pcid' => $input['ProduitClientID']
		);
		// $query = "EXEC genProduitStatSite {$input['sid']}";
		//
		// doRequest($query);

		$ch = curl_init('http://10.199.54.224/avis-produits/avp-generator/index.php');

		curl_setopt($ch, CURLOPT_FORBID_REUSE, TRUE);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_VERBOSE, TRUE);
		curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);
		curl_setopt($ch, CURLINFO_HEADER_OUT, TRUE);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT_MS, 10000);
		curl_setopt($ch, CURLOPT_TIMEOUT, 1000);
		curl_setopt($ch, CURLOPT_TIMEOUT_MS, 1000000);
		curl_setopt($ch, CURLOPT_PROTOCOLS, CURLPROTO_HTTP | CURLPROTO_HTTPS);

		$response = curl_exec($ch);

		curl_close($ch);
		var_export($response);

	}

	function getAuth(&$input) {
		$data = array();

		$sid = $input['SiteID'];
		$pcid = $input['ProduitClientID'];
		$nb = $input['NbPerPage'];

		getAuthorizedSites($sites, $sid);

		if (count($sites)) {
			$query = "
				SELECT Peka
				FROM rating..RatingSiteAccess RSA
				WHERE RSA.SiteID = {$sid} AND RSA.isValide = 1
			";
			// var_dump($query);
			$results = doRequest($query, 'Peka');
			$password = $results[0]['Peka'];
			$key = cipher_md5(sprintf('%d_sceau_%s', $sid, $password));
			$str = $sid.'/'.$pcid.'/'.$nb.'/'.$key;
			$data['hash'] = base64_encode(hash_hmac('sha256', $str, $key, true));
		}
		return ($data);
	}

	try {
		$error = null;
		$Params = $_GET['Params'];
		if (empty($Params)) {
			throw new Exception('Bad Params');
		}
		$Type = $Params['Type'];
		if (empty($Type)) {
			throw new Exception('Bad Type');
		}
		// var_export($Params);
		// error_reporting(E_ALL);
		// ini_set('xdebug.var_display_max_depth', 5);
		// ini_set('xdebug.var_display_max_children', 256);
		// ini_set('xdebug.var_display_max_data', 4096);
		$funcName = 'get' . ucfirst(strtolower($Type));
		// var_export($funcName);
		if (!empty($Params['Query'])) {
			$Data = $Params['Query'];
		}
		$results = $funcName($Data);
		// var_export($query);
		if (is_string($results)) {
			$results = doRequest($results);
		}
		json_encode_with_header($results);

	} catch (DatabaseException $e) {
		$error = $e;
	} catch (Exception $e) {
		$error = $e;
	}

	if($error) {
		sendError($error);
	}
