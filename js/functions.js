var validator = {
	form: null,
	initialized: false,
	init: function (selector) {
		this.form = selector;

		this.form.formValidation({
			framework: 'bootstrap',
			icon: false,
			err: {
				container: 'tooltip'
			},
			live: 'disabled',
			fields: {
				SiteID: {
					validators: {
						notEmpty: {
							message: 'Veuillez choisir un site marchand',
						}
					}
				},
				ProduitClientID: {
					validators: {
						notEmpty: {
							message: 'Veuillez choisir un produit',
						}
					}
				}
			}
		});

		this.initialized = true;

	},
	exec: function (field) {
		if (!this.initialized) {
			return false;
		}
		let result;
		this.reset(field);
		if (!field) {
			this.form
				.data('formValidation')
				.validate();

			result = this.form.data('formValidation').isValid();
		} else {
			this.form
				.data('formValidation')
				.validateField(field);

			result = this.form.data('formValidation').isValidField(field);
		}

		return result;

	},
	setOption: function(field, validator, type, value) {
		this.form
			.data('formValidation')
			.updateOption(field, validator, type, value);
	},
	clear: function() {
		// $('#log').empty()
		// 	.addClass('noDisplay');
		// $('#url').val('');
		// this.setOption('SiteID', 'notEmpty', 'enabled', false);
		// $('#SiteID').val(-1)
		// 	.trigger('change');
		// this.setOption('SiteID', 'notEmpty', 'enabled', true);
		// this.reset();
	},
	reset: function(field) {
		if (!field) {
			this.form
				.data('formValidation')
				.resetForm();
		} else {
			this.form
				.data('formValidation')
				.resetField(field);
		}
	}

};


var cooks = {
	name: null,
	content: null,
	init: function(name) {
		this.name = name;
		this.load();
	},
	load: function() {
		if (Cookies.get(this.name)) {
			this.content = new Set($.parseJSON(Cookies.get(this.name)));
		} else {
			this.content = new Set;
		}
	},
	update: function() {
		Cookies.set(this.name, this.get());
		this.load();
	},
	add: function(value) {
		this.content.add(value);
		this.update();
	},
	remove: function(value) {
		this.content.delete(value);
		this.update();
	},
	get: function() {
		return Array.from(this.content);
	}
};


function printDebug(json) {
	var index, trace, results;
	for (trace in json.trace) {
		if ('table' in console) { // detection de la presence de la methode 'table' dans l'object console
			var labels = new Array(), content = {};
			results = {};
			for (index in json.trace[trace]) {
				labels.push(index);
				content[index] = json.trace[trace][index];
			}
			results['trace #' + trace] = content;
			console.table(results, labels);
		} else {
			for (index in json.trace[trace]) {
				console.log(index + ' = ' + json.trace[trace][index]);
			}
		}
	}
	delete json.trace;
	for (index in json) {
		if (json[index] && typeof json[index] === 'string') {
			trace = $.parseHTML(json[index]);
			results = '';
			for (var i = 0; i < trace.length; i++) {
				if ('data' in trace[i]) {
					results += trace[i].data;
				} else {
					results += $(trace[i].outerHTML.replace(/<br>/gi, '\n')).text();
				}
			}
			console.log(index + ':\n' + results + '\n');
		}
	}
}

function doAjax(container) {

	return $.ajax({
		type : container.type,
		url : container.url ? container.url : 'server/index.php',
		data : $.param(container.data),
		cache : container.cache ? true : false,
		beforeSend : container.beforeSend,
		success : function (json, textStatus, jqXHR) {
			if (container.success) {
				container.success(json.Data, this, container.offlineData);
			}
		},
		error : function (jqXHR, textStatus, errorThrown) {
			if (jqXHR.responseText) {
				var json = $.parseJSON(jqXHR.responseText);
				printDebug(json);
			}
			if (container.error) {
				container.error(jqXHR, textStatus, errorThrown);
			}
			alert("Une erreur s'est produite : " + errorThrown);
		},
		complete : function (jqXHR, textStatus) {
			if (container.complete) {
				container.complete(jqXHR,textStatus);
			}
		}
	});
}

function initialize() {
	$.fn.select2.defaults.set('debug', true);

	function getSites() {
		doAjax({
			type : 'GET',
			url: 'server/index.php',
			data : {
				Params: {
					Type: 'sites'
				}
			}
		})
		.done(function(response) {
			let options = getOptions({
				data: response,
				placeholder: {
					id: '-1',
					text: 'Choisir un site'
				}
			});
			$('#SiteID')
				.select2(options)
				.val(null)
				.trigger('change');

			// console.log(response);
		});
	}
	function getProducts() {
		doAjax({
			type : 'GET',
			url: 'server/index.php',
			data : {
				Params: {
					Type: 'products',
					Query: {
						SiteID: $('#SiteID').val()
					}
				}
			},
			beforeSend: function() {
				$('#ProduitClientID').empty();
			}
		}).done(function(response) {
			let options = getOptions({
				data: response,
				placeholder: {
					id: '-1',
					text: 'Choisir un produit'
				}
			});
			$('#ProduitClientID')
				.select2(options)
				.val(null)
				.trigger('change');
		});
	}


	let doOption = function(data, container) {

		let span = $('<span></span>').clone(true);
		if (data.img) {
			let img = $('<img>').clone(true);
			img.attr('src', data.img)
			.css({
				'display': 'inline-block',
				'margin-right': 10
			});

			span.html(img)
		}

		let p = $('<p></p>').clone(true);
		p.text(data.text)
			.css({
				'display': 'inline-block'
			});


		span.append(p);
		return span;
	};
	let getOptions = function (options) {
		let defaultOpts = {
			theme: 'bootstrap',
			templateResult: doOption,
			templateSelection: doOption,
			width: '100%',
			allowClear: true
		};
		return Object.assign(options, defaultOpts);
	};

	function initSiteSelector() {
		let options = getOptions({
			placeholder: {
				id: '-1',
				text: 'Choisir un site'
			}
		});
		$('#SiteID')
			.select2(options)
			.on('select2:select', function() {
				getProducts();
			});
			getSites();
	}

	function initProductSelector() {
		let options = getOptions({
			placeholder: {
				id: '-1',
				text: 'Choisir un produit'
			}
		});
		$('#ProduitClientID').select2(options);
	}

	function initSiteUrlSelector() {
		let options = getOptions({
			data: [
				{ id: 0, text: 'http://10.199.54.224/avis-produits/index.php' },
				{ id: 1, text: 'https://www.fia-net.com/avis-produits/index.php' }
			],
			placeholder: {
				id: 0,
				text: 'http://10.199.54.224/avis-produits/index.php'
			},
			minimumResultsForSearch: Infinity
		});
		$('#SiteUrl').select2(options);
	}
	initSiteSelector();
	initProductSelector();
	initSiteUrlSelector();
}

$(
	function() {
		initialize();

		validator.init($('#avisProduitsForm'));
		$('#loadAvis').click(function() {
			let isValid = validator.exec();
			if (!isValid) {
				return;
			}
			doAjax({
				type : 'GET',
				url: 'server/index.php',
				data : {
					Params: {
						Type: 'auth',
						Query: {
							SiteID: $('#SiteID').val(),
							ProduitClientID: $('#ProduitClientID').val(),
							NbPerPage: $('#NbPerPage').val()
						}
					}
				}
			})
			.done(function(response) {
				let data = {
					sid: $('#SiteID').val(),
					pcid: $('#ProduitClientID').val(),
					nb: $('#NbPerPage').val(),
					auth: response.hash
				};

				let url = $('#SiteUrl').select2('data')[0].text + '?' + $.param(data);
				window.open(url, 'AvisProduits', 'width=1440, height=900');
			});
		});

		$('#generateAvis').click(function() {
			doAjax({
				type : 'GET',
				url: 'server/index.php',
				data : {
					Params: {
						Type: 'generateAvis',
						Query: {
							ProduitClientID: $('#ProduitClientID').val(),
							SiteID: $('#SiteID').val()
						}
					}
				}
			});
		});

		$('.clearForm').each(function() {
			$(this).click(function() {
				$('select').val(null).trigger('change');
			});
		});
	}
);
